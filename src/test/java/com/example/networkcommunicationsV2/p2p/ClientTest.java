package com.example.networkcommunicationsV2.p2p;

import com.alibaba.fastjson.JSON;
import com.example.networkcommunicationsV2.entity.ClientTcpInfo;
import com.example.networkcommunicationsV2.entity.TCPMessage;
import com.googlecode.catchexception.apis.BDDCatchException;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.*;
import java.util.LinkedList;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;

import static com.example.networkcommunicationsV2.entity.ClientTcpInfo.MessageType.BROADCAST;
import static com.example.networkcommunicationsV2.entity.ClientTcpInfo.MessageType.REPLY;
import static com.example.networkcommunicationsV2.entity.TCPMessage.MessageType.*;
import static com.googlecode.catchexception.apis.BDDCatchException.caughtException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;


@ExtendWith(MockitoExtension.class)
class ClientTest {
    private static int notUsedPort = 9999;
    private static int usedPort = 9998;
    @Mock
    DatagramSocket datagramSocket;
    @Mock
    ServerSocket serverSocket;
    @InjectMocks
    Client client;

    private ClientTcpInfo udpThrowErrorMsg = ClientTcpInfo.builder().id("error").ip("1.1.1").port(22).messageType(BROADCAST).build();

    private Socket throwIOExceptionSocket = new Socket("www.baidu.com", 80);

    ClientTest() throws IOException {
    }


    //startTCPConnectionListener
    @Test
    void should_invoke_accept_method_1_times_on_serverSocket_when_startTCPConnectionListener_given_unused_port() throws IOException, InterruptedException {
        mockReceiveTcpConnectionTemplate(sockets -> {
        });

        client.startTCPConnectionListener(notUsedPort);
        Thread.sleep(100);

        then(serverSocket).should().accept();
    }

    @Test
    void should_BindException_when_startTCPConnectionListener_given_used_port() throws IOException {
        mockTcpPortInUse(usedPort);
        //doThrow(BindException.class).when(serverSocket).bind(any());

        BDDCatchException.when(() -> client.startTCPConnectionListener(usedPort));

        BDDAssertions.then(caughtException()).isInstanceOf(BindException.class);
    }

    @Test
    void should_invoke_accept_method_1_times_on_serverSocket_when_repeat_startTCPConnectionListener() throws IOException, InterruptedException {
        mockReceiveTcpConnectionTemplate(sockets -> {
        });

        client.startTCPConnectionListener(notUsedPort);
        client.startTCPConnectionListener(notUsedPort);
        Thread.sleep(100);

        then(serverSocket).should().accept();
    }

    //startUDPMessageListener
    @Test
    void should_invoke_receive_method_1_times_on_datagramSocket_when_startUDPMessageListener_given_unused_port() throws IOException, InterruptedException {
        mockUdpReceiveBlocking();

        client.startUDPMessageListener(notUsedPort);
        Thread.sleep(100);

        then(datagramSocket).should().receive(any());
    }

    @Test
    void should_BindException_when_startUDPMessageListener_given_used_port() throws SocketException {
        mockUdpPortInUse(usedPort);

        BDDCatchException.when(() -> client.startUDPMessageListener(usedPort));

        BDDAssertions.then(caughtException()).isInstanceOf(BindException.class);
    }

    @Test
    void should_invoke_receive_method_1_times_on_datagramSocket_when_repeat_startUDPConnectionListener_given_unused_port() throws IOException, InterruptedException {
        mockUdpReceiveBlocking();

        client.startUDPMessageListener(notUsedPort);
        client.startUDPMessageListener(notUsedPort);
        Thread.sleep(100);

        then(datagramSocket).should().receive(any());
    }


    //stopTCPConnectionListener
    @Test
    void should_invoke_close_method_1_times_on_serverSocket_when_stopTCPConnectionListener_given_has_startTCPConnectionListener() throws IOException {
        client.startTCPConnectionListener(notUsedPort);

        client.stopTCPConnectionListener();

        then(serverSocket).should().close();
    }

    @Test
    void should_never_invoke_close_method_on_serverSocket_when_stopTCPConnectionListener_given_no_startTCPConnectionListener() throws IOException {

        client.stopTCPConnectionListener();

        then(serverSocket).should(never()).close();
    }

    @Test
    void should_invoke_close_method_1_times_on_serverSocket_when_repeat_stopTCPConnectionListener_given_has_startTCPConnectionListener() throws IOException {
        client.startTCPConnectionListener(notUsedPort);

        client.stopTCPConnectionListener();
        client.stopTCPConnectionListener();

        then(serverSocket).should().close();
    }

    //stopUDPListener
    @Test
    void should_invoke_close_method_1_times_on_datagramSocket_when_stopUDPListener_given_has_startUDPMessageListener() {
        client.startUDPMessageListener(notUsedPort);

        client.stopUdpListener();

        then(datagramSocket).should().close();
    }

    @Test
    void should_never_invoke_close_of_datagramSocket_when_stopUDPMessageListener_given_no_startUDPMessageListener() {

        client.stopUdpListener();

        then(datagramSocket).should(never()).close();
    }

    @Test
    void should_invoke_close_method_1_times_on_datagramSocket_when_repeat_stopUDPMessageListener_given_has_startUDPMessageListener() {
        client.startUDPMessageListener(notUsedPort);

        client.stopUdpListener();
        client.stopUdpListener();

        then(datagramSocket).should().close();
    }

    //sendBroadcastMessage
    @Test
    void should_invoke_send_method_1_times_on_datagramSocket_when_sendBroadcastMessage_given_1_message() throws IOException, InterruptedException {
        ClientTcpInfo clientTcpInfo = ClientTcpInfo.builder().ip("testIp").port(22).build();
        ArgumentCaptor<DatagramPacket> argument = ArgumentCaptor.forClass(DatagramPacket.class);

        client.sendBroadcastMessage(clientTcpInfo);
        Thread.sleep(500);

        then(datagramSocket).should().send(argument.capture());
        ClientTcpInfo sendMsg = JSON.parseObject(new String(argument.getValue().getData()), ClientTcpInfo.class);
        BDDAssertions.then(sendMsg.getMessageType()).isEqualTo(BROADCAST);
        BDDAssertions.then(sendMsg.getIp()).isEqualTo("testIp");
        BDDAssertions.then(sendMsg.getPort()).isEqualTo(22);
    }

    //receive_message
    @Test
    void should_never_invoke_send_method_on_datagramSocket_when_receive_broadcast_message_given_0_message() throws IOException {
        mockReceiveMessageTemplate(clientTcpInfos -> {
        });

        client.startUDPMessageListener(notUsedPort);

        then(datagramSocket).should(never()).send(any());
        then(spy(client)).should(never()).createAndSaveConnectionBy(any());
    }

    @Test
    void should_invoke_send_method_3_times_on_datagramSocket_when_receive_broadcast_message_given_4_message_contain_1_ThrowErrorMsg() throws IOException, InterruptedException {
        mockTcpLocalAddress();
        mockReceiveMessageTemplate(datasource -> {
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("1.1.1").port(11).messageType(BROADCAST).build());
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("1.1.1").port(22).messageType(BROADCAST).build());
            datasource.add(udpThrowErrorMsg);
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("1.1.1").port(44).messageType(BROADCAST).build());
        });
        Client spyClient = spy(client);

        spyClient.startUDPMessageListener(notUsedPort);
        Thread.sleep(1000);

        then(spyClient).should(times(3)).createAndSaveConnectionBy(any());
        then(datagramSocket).should(times(3)).send(any());
    }

    @Test
    void should_invoke_send_method_1_times_on_datagramSocket_when_receive_broadcast_message_given_2_message_contain_1_IllegalMessage() throws IOException, InterruptedException {
        mockTcpLocalAddress();
        mockReceiveIllegalMessage(dataSources -> {
            dataSources.add("111");
            dataSources.add(JSON.toJSONString(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("1.1.1").port(22).messageType(BROADCAST).build()));
        });
        Client spyClient = spy(client);

        spyClient.startUDPMessageListener(notUsedPort);
        Thread.sleep(1000);

        then(spyClient).should().createAndSaveConnectionBy(any());
        then(datagramSocket).should().send(any());
    }

    //receive_broadcast_message

    @Test
    void should_invoke_send_method_3_times_on_datagramSocket_when_receive_broadcast_message_given_3_message() throws IOException, InterruptedException {
        mockTcpLocalAddress();
        mockReceiveMessageTemplate(datasource -> {
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("1.1.1").port(11).messageType(BROADCAST).build());
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("1.1.1").port(22).messageType(BROADCAST).build());
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("1.1.1").port(44).messageType(BROADCAST).build());
        });
        Client spyClient = spy(client);

        spyClient.startUDPMessageListener(notUsedPort);
        Thread.sleep(1000);

        BDDAssertions.then(client.getCountOfPartnerTcpInfo()).isEqualTo(3);
        then(spyClient).should(times(3)).createAndSaveConnectionBy(any());
        then(datagramSocket).should(times(3)).send(any());
    }


    @Test
    void should_invoke_send_method_1_times_on_datagramSocket_when_receive_broadcast_message_given_repeat_msg() throws IOException, InterruptedException {
        ArgumentCaptor<DatagramPacket> argument = ArgumentCaptor.forClass(DatagramPacket.class);
        InetSocketAddress mockLocalSocketAddress = mockTcpLocalAddress();
        String msgId = UUID.randomUUID().toString();
        ClientTcpInfo receiveMsg = ClientTcpInfo.builder().id(msgId).ip("1.1.1").port(usedPort).messageType(BROADCAST).build();
        mockReceiveMessageTemplate(datasource -> {
            datasource.add(receiveMsg);
            datasource.add(receiveMsg);
            datasource.add(receiveMsg);
        });
        Client spy = spy(client);

        spy.startUDPMessageListener(notUsedPort);
        Thread.sleep(1000);

        then(datagramSocket).should().send(argument.capture());
        then(spy).should().createAndSaveConnectionBy(any());
        ClientTcpInfo sendMsg = JSON.parseObject(new String(argument.getValue().getData()), ClientTcpInfo.class);
        BDDAssertions.then(sendMsg.getMessageType()).isEqualTo(REPLY);
        BDDAssertions.then(sendMsg.getIp()).isEqualTo(mockLocalSocketAddress.getHostString());
        BDDAssertions.then(sendMsg.getPort()).isEqualTo(mockLocalSocketAddress.getPort());
    }

    //receive_reply_message
    @Test
    void should_3_size_of_PartnerTcpInfo_when_receive_message_given_3_reply_message() throws IOException, InterruptedException {
        mockReceiveMessageTemplate(datasource -> {
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("127.0.0.1").port(11).messageType(REPLY).build());
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("127.0.0.1").port(22).messageType(REPLY).build());
            datasource.add(ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("127.0.0.1").port(33).messageType(REPLY).build());
        });
        Client spy = spy(client);

        spy.startUDPMessageListener(notUsedPort);
        Thread.sleep(1000);

        BDDAssertions.then(spy.getCountOfPartnerTcpInfo()).isEqualTo(3);
        then(spy).should(times(3)).createAndSaveConnectionBy(any());
    }

    @Test
    void should_1_size_of_PartnerTcpInfo_when_receive_message_given_3_repeat_reply_message() throws IOException, InterruptedException {
        ClientTcpInfo repeatMsg = ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("test:ip").port(11).messageType(REPLY).build();
        mockReceiveMessageTemplate(datasource -> {
            datasource.add(repeatMsg);
            datasource.add(repeatMsg);
            datasource.add(repeatMsg);
        });
        Client spy = spy(client);

        spy.startUDPMessageListener(notUsedPort);
        Thread.sleep(500);

        BDDAssertions.then(client.getCountOfPartnerTcpInfo()).isEqualTo(1);
        then(spy).should().createAndSaveConnectionBy(any());
    }

    //create_connect
    @Test
    void should_1_size_of_PartnerTcpForWrite_when_create_connect_given_valid_ClientTcpInfo() throws InterruptedException {
        ClientTcpInfo validClientTcpInfo = ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("www.baidu.com").port(80).messageType(REPLY).build();

        client.createAndSaveConnectionBy(validClientTcpInfo);
        Thread.sleep(500);

        BDDAssertions.then(client.getCountOfPartnerTcpForWrite()).isEqualTo(1);
    }

    @Test
    void should_0_size_of_PartnerTcpForWrite_when_create_connect_given_invalid_ClientTcpInfo() throws InterruptedException {
        ClientTcpInfo validClientTcpInfo = ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("1.1.1").port(usedPort).messageType(REPLY).build();

        client.createAndSaveConnectionBy(validClientTcpInfo);
        Thread.sleep(1000);

        BDDAssertions.then(client.getCountOfPartnerTcpForWrite()).isEqualTo(0);
    }

    @Test
    void should_1_size_of_PartnerTcpForWrite_when_create_connect_given_2_ClientTcpInfo_contain_same_ip_port() throws InterruptedException {
        String ip = "www.baidu.com";
        int port = 80;
        ClientTcpInfo validClientTcpInfo = ClientTcpInfo.builder().ip(ip).port(port).build();
        ClientTcpInfo validClientTcpInfo2 = ClientTcpInfo.builder().ip(ip).port(port).build();

        client.createAndSaveConnectionBy(validClientTcpInfo);
        client.createAndSaveConnectionBy(validClientTcpInfo2);
        Thread.sleep(500);

        BDDAssertions.then(client.getCountOfPartnerTcpForWrite()).isEqualTo(1);
    }

    //receiveTcpConnection
    @Test
    void should_1_size_of_PartnerTcpForRead_when_receive_connect_given_1_connection() throws IOException, InterruptedException {
        mockReceiveTcpConnectionTemplate(sockets -> {
            try {
                sockets.add(new Socket("www.baidu.com", 80));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        client.startTCPConnectionListener(notUsedPort);
        Thread.sleep(300);

        BDDAssertions.then(client.getCountOfPartnerTcpForRead()).isEqualTo(1);
    }

    @Test
    void should_0_size_of_PartnerTcpForRead_when_receive_connect_given_0_connection() throws IOException, InterruptedException {
        mockReceiveTcpConnectionTemplate(sockets -> {
        });

        client.startTCPConnectionListener(notUsedPort);
        Thread.sleep(300);

        BDDAssertions.then(client.getCountOfPartnerTcpForRead()).isEqualTo(0);
    }

    @Test
    void should_1_size_of_PartnerTcpForRead_when_receive_connect_given_2_connection_contain_1_exception_connection() throws IOException, InterruptedException {
        mockReceiveTcpConnectionTemplate(sockets -> {
            sockets.add(throwIOExceptionSocket);
            try {
                sockets.add(new Socket("www.baidu.com", 80));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        client.startTCPConnectionListener(notUsedPort);
        Thread.sleep(300);

        BDDAssertions.then(client.getCountOfPartnerTcpForRead()).isEqualTo(1);
    }

    // monitorTcpMessage
    @Test
    void should_1_size_of_tcpMessageQueue_when_monitorTcpMessage_given_one_valid_message() throws IOException, InterruptedException {
        Socket socket = new Socket("www.baidu.com", 80);
        socket = spy(socket);
        mockReadInputStreamTemplate(socket, dataSource -> dataSource.add(JSON.toJSONString(TCPMessage.builder().from("1").messageType(NORMAL).build())));

        client.monitorTcpMessage(socket);
        Thread.sleep(1000);

        BDDAssertions.then(client.getTcpMessageQueueCount()).isEqualTo(1);
    }

    @Test
    void should_1_size_of_tcpMessageQueue_when_monitorTcpMessage_given_2_message_contain_1_invalid_message() throws IOException, InterruptedException {
        Socket socket = new Socket("www.baidu.com", 80);
        socket = spy(socket);
        mockReadInputStreamTemplate(socket, dataSource -> {
            dataSource.add("not TCPMessage json string");
            dataSource.add(JSON.toJSONString(TCPMessage.builder().messageType(NORMAL).build()));
        });

        client.monitorTcpMessage(socket);
        Thread.sleep(1000);

        BDDAssertions.then(client.getTcpMessageQueueCount()).isEqualTo(1);
    }

    @Test
    void should_0_size_of_tcpMessageQueue_when_monitorTcpMessage_given_0_message() throws IOException, InterruptedException {
        Socket socket = new Socket("www.baidu.com", 80);
        socket = spy(socket);
        mockReadInputStreamTemplate(socket, dataSource -> {

        });

        client.monitorTcpMessage(socket);
        Thread.sleep(1000);

        BDDAssertions.then(client.getTcpMessageQueueCount()).isEqualTo(0);
    }

    @Test
    void should_close_socket_when_monitorTcpMessage_given_IOException() throws IOException, InterruptedException {
        Socket socket = new Socket("www.baidu.com", 80);
        socket = spy(socket);
        mockReadInputStreamTemplate(socket, dataSource -> dataSource.add("^"));
        //when(socket.getInputStream().read()).thenThrow(IOException.class); 无法生效

        client.monitorTcpMessage(socket);
        Thread.sleep(1000);

        BDDAssertions.then(socket.isClosed()).isEqualTo(true);
        BDDAssertions.then(client.getCountOfPartnerTcpForWrite()).isEqualTo(0);
    }

    @Test
    void should_0_size_of_tcpMessageQueue_when_monitorTcpMessage_given_one_valid_ack_message() throws IOException, InterruptedException {
        Socket socket = new Socket("www.baidu.com", 80);
        socket = spy(socket);
        mockReadInputStreamTemplate(socket, dataSource -> dataSource.add(JSON.toJSONString(TCPMessage.builder().messageType(ACK).build())));

        client.monitorTcpMessage(socket);
        Thread.sleep(1000);

        BDDAssertions.then(client.getTcpMessageQueueCount()).isEqualTo(0);
    }


    //sendTcpMessage
    @Test
    void should_Exception_when_sendTcpMessage_given_not_exist_des_address() throws UnknownHostException {
        mockTcpLocalAddress();

        BDDCatchException.when(() -> client.sendTcpMessage("test:ip", 22, "test"));

        BDDAssertions.then(caughtException()).isInstanceOf(RuntimeException.class);
        BDDAssertions.then(client.getTcpSendingMap().elements().nextElement().getMessageType()).isEqualTo(TARGET_NOT_EXIST);
    }

    @Test
    void should_success_when_sendTcpMessage_given_exist_des_address() throws InterruptedException, UnknownHostException {
        mockTcpLocalAddress();
        ClientTcpInfo validClientTcpInfo = ClientTcpInfo.builder().id(UUID.randomUUID().toString()).ip("www.baidu.com").port(80).messageType(REPLY).build();
        client.createAndSaveConnectionBy(validClientTcpInfo);
        Thread.sleep(500);

        client.sendTcpMessage("www.baidu.com", 80, "test");

        BDDAssertions.then(client.getTcpSendingMapCount()).isEqualTo(1);
        BDDAssertions.then(client.getTcpSendingMap().elements().nextElement().getMessageType()).isEqualTo(NORMAL);
    }

    //集成测试
    @Test
    void should_not_receive_self_BroadcastMessage_when_start() throws InterruptedException {
        Client client1 = new Client();
        client1.start(9999, 11111);
        Thread.sleep(500);

        BDDAssertions.then(client1.getCountOfPartnerTcpInfo()).isEqualTo(0);
        BDDAssertions.then(client1.getCountOfPartnerTcpForWrite()).isEqualTo(0);

        client1.shutdown();
    }

    @Test
    void should_create_connect_when_client_2_start_given_client_1_start_first() throws InterruptedException {
        Client client1 = new Client();
        client1.start(9999, 11111);
        Thread.sleep(500);

        Client client2 = new Client();
        client2.start(22222, 22222);
        Thread.sleep(1000);

        BDDAssertions.then(client1.getCountOfPartnerTcpInfo()).isEqualTo(1);
        BDDAssertions.then(client2.getCountOfPartnerTcpInfo()).isEqualTo(1);
        BDDAssertions.then(client1.getCountOfPartnerTcpForWrite()).isEqualTo(1);
        BDDAssertions.then(client2.getCountOfPartnerTcpForWrite()).isEqualTo(1);

        client1.shutdown();
        client2.shutdown();
    }

    @Test
    void should_client_2_receive_1_message_when_client_1_send_message_to_client_2_given_client_1_start_and_client_2_start() throws InterruptedException {
        Client client1 = new Client();
        client1.start(9999, 11111);
        Thread.sleep(500);
        Client client2 = new Client();
        client2.start(22222, 22222);
        Thread.sleep(500);

        client1.sendTcpMessage("0.0.0.0", 22222, "test");
        BDDAssertions.then(client1.getTcpSendingMapCount()).isEqualTo(1);

        Thread.sleep(100);
        BDDAssertions.then(client2.getTcpMessageQueueCount()).isEqualTo(1);
        BDDAssertions.then(client1.getTcpSendingMapCount()).isEqualTo(0);//收到ack 消息对冲

        client1.shutdown();
        client2.shutdown();
    }

    @Test
    void should_client_1_receive_1_message_when_client_2_send_message_to_client_1_given_client_1_start_and_client_2_start() throws InterruptedException {
        Client client1 = new Client();
        client1.start(9999, 11111);
        Thread.sleep(500);
        Client client2 = new Client();
        client2.start(22222, 22222);
        Thread.sleep(500);

        client2.sendTcpMessage("0.0.0.0", 11111, "test");

        BDDAssertions.then(client2.getTcpSendingMapCount()).isEqualTo(1);
        Thread.sleep(100);
        BDDAssertions.then(client1.getTcpMessageQueueCount()).isEqualTo(1);
        BDDAssertions.then(client2.getTcpSendingMapCount()).isEqualTo(0);//消息对冲

        client1.shutdown();
        client2.shutdown();
    }

    @Test
    void should_remove_PartnerTcpForRead_when_client_2_shutdown_given_client_1_start_and_client_2_start() throws InterruptedException {
        Client client1 = new Client();
        client1.start(9999, 11111);
        Thread.sleep(500);
        Client client2 = new Client();
        client2.start(22222, 22222);
        Thread.sleep(500);
        client2.shutdown();
        Thread.sleep(300);

        BDDAssertions.then(client1.getCountOfPartnerTcpForRead()).isEqualTo(0);

        client1.shutdown();
    }

    @Test
    void should_Exception_and_remove_client2_info_when_client_1_send_message_to_client_2_given_client_1_start_and_client_2_start_and_client_2_shutdown() throws InterruptedException {
        Client client1 = new Client();
        client1.start(9999, 11111);
        Thread.sleep(500);
        Client client2 = new Client();
        client2.start(22222, 22222);
        Thread.sleep(500);

        client2.shutdown();
        Thread.sleep(300);

        BDDCatchException.when(() -> client1.sendTcpMessage("0.0.0.0", 22222, "test"));

        BDDAssertions.then(caughtException()).isInstanceOf(SocketException.class);
        BDDAssertions.then(client1.getCountOfPartnerTcpForWrite()).isEqualTo(0);
        BDDAssertions.then(client1.getSocket2ByteBufferCount()).isEqualTo(0);
        BDDAssertions.then(client1.getTcpSendingMap().elements().nextElement().getMessageType()).isEqualTo(TARGET_OFFLINE);

        client1.shutdown();
        client2.shutdown();
    }


    private int mockReceiveMessageTemplate(Consumer<LinkedList<ClientTcpInfo>> callback) throws IOException {
        LinkedList<ClientTcpInfo> datasource = new LinkedList<>();
        callback.accept(datasource);
        datasource.add(null);
        overWriteMethodOfUDPReceive(datasource);
        return datasource.size() - 1;
    }

    private void mockReceiveIllegalMessage(Consumer<LinkedList<String>> callback) throws IOException {
        LinkedList<String> datasource = new LinkedList<>();
        callback.accept(datasource);
        datasource.add(null);
        doAnswer(invocation -> {
            DatagramPacket argument = invocation.getArgument(0, DatagramPacket.class);
            String clientTcpInfoStr = datasource.remove();
            if (clientTcpInfoStr == null) {
                synchronized (datasource) {
                    datasource.wait();
                }
            }
            argument.setData(Objects.requireNonNull(clientTcpInfoStr).getBytes());
            argument.setSocketAddress(new InetSocketAddress(Inet4Address.getLocalHost(), notUsedPort));
            return null;
        }).when(datagramSocket).receive(any(DatagramPacket.class));
    }

    private void overWriteMethodOfUDPReceive(LinkedList<ClientTcpInfo> datasource) throws IOException {
        doAnswer(invocation -> {
            DatagramPacket argument = invocation.getArgument(0, DatagramPacket.class);
            ClientTcpInfo clientTcpInfoStr = datasource.remove();
            if (udpThrowErrorMsg == clientTcpInfoStr) {
                throw new IOException();
            }
            if (clientTcpInfoStr == null) {
                synchronized (datasource) {
                    datasource.wait();
                }
            }
            argument.setData(JSON.toJSONString(clientTcpInfoStr).getBytes());
            argument.setSocketAddress(new InetSocketAddress(Inet4Address.getLocalHost(), notUsedPort));
            return null;
        }).when(datagramSocket).receive(any(DatagramPacket.class));
    }

    private int mockReceiveTcpConnectionTemplate(Consumer<LinkedList<Socket>> callback) throws IOException {
        LinkedList<Socket> datasource = new LinkedList<>();
        callback.accept(datasource);
        datasource.add(null);
        overWriteMethodOfTcpConnectionAcceptReceive(datasource);
        return datasource.size() - 1;
    }

    private void overWriteMethodOfTcpConnectionAcceptReceive(LinkedList<Socket> datasource) throws IOException {
        doAnswer(invocation -> {
            Socket socket = datasource.remove();
            if (throwIOExceptionSocket == socket) {
                throw new IOException();
            }
            if (socket == null) {
                synchronized (datasource) {
                    datasource.wait();
                }
            }
            return socket;
        }).when(serverSocket).accept();
    }

    private int mockReadInputStreamTemplate(Socket socket, Consumer<LinkedList<String>> callback) throws IOException {
        LinkedList<String> datasource = new LinkedList<>();
        callback.accept(datasource);
        datasource.add("!");
        overWriteMethodOfInputStreamRead(socket, datasource);
        return datasource.size();
    }

    private void overWriteMethodOfInputStreamRead(Socket socket, LinkedList<String> datasource) throws IOException {
        String data = String.join("\n", datasource);
        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(data.getBytes());
        ByteArrayInputStream spy = spy(byteInputStream);
        doAnswer(invocation -> {
            doAnswer(invocation1 -> {
                int read = byteInputStream.read();
                byte waitFlag = "!".getBytes()[0];
                byte errorFlag = "^".getBytes()[0];
                if (errorFlag == (read & 0xff)) {
                    throw new IOException();
                }
                if (waitFlag == (read & 0xff)) {
                    synchronized (spy) {
                        spy.wait();
                    }
                }
                return read;
            }).when(spy).read();
            return spy;
        }).when(socket).getInputStream();
    }

    private void mockUdpPortInUse(int port) throws SocketException {
        doAnswer(invocation -> {
            InetSocketAddress argument1 = invocation.getArgument(0);
            if (argument1.getPort() == port) {
                throw new BindException();
            }
            return null;
        }).when(datagramSocket).bind(any());

    }

    private void mockTcpPortInUse(int port) throws IOException {
        doAnswer(invocation -> {
            InetSocketAddress argument1 = invocation.getArgument(0);
            if (argument1.getPort() == port) {
                throw new BindException();
            }
            return null;
        }).when(serverSocket).bind(any());
    }

    private void mockUdpReceiveBlocking() throws IOException {

        doAnswer(invocation -> {
            synchronized (this) {
                this.wait();
            }
            return null;
        }).when(datagramSocket).receive(any(DatagramPacket.class));
    }

    private InetSocketAddress mockTcpLocalAddress() throws UnknownHostException {
        InetSocketAddress mockLocalSocketAddress = new InetSocketAddress(Inet4Address.getLocalHost().getHostAddress(), notUsedPort);
        when(serverSocket.getLocalSocketAddress()).thenReturn(mockLocalSocketAddress);

        return mockLocalSocketAddress;
    }
}
