package com.example.networkcommunicationsV1.service;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class TCPServiceTest {

    @RepeatedTest(100)
    void should_1_size_when_startConnectionListener_given_one_client_to_connect() throws InterruptedException {
        int servicePort1 = 9001;
        TCPService tcpService1 = new TCPService(servicePort1);
        tcpService1.startConnectionListener();

        CountDownLatch countDownLatch = new CountDownLatch(1);
        Thread clientThread = new Thread(() -> {
            Socket socket = new Socket();
            try {
                socket.connect(new InetSocketAddress("127.0.0.1", servicePort1));
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        });
        clientThread.start();
        countDownLatch.await();
        Thread.sleep(10);
        tcpService1.stopConnectionListener();
        assertThat(tcpService1.getCountOfPartnerSocket()).isEqualTo(1);
    }

    @RepeatedTest(100)
    void should_IOException_when_stopConnectionListener_given_one_client_to_connect() {
        int servicePort1 = 9001;
        TCPService tcpService1 = new TCPService(servicePort1);
        tcpService1.startConnectionListener();

        tcpService1.stopConnectionListener();

        assertThatExceptionOfType(IOException.class).isThrownBy(() -> {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress("127.0.0.1", servicePort1));
        });
    }

    @RepeatedTest(100)
    void should_1_message_when_startMessageListener_given_send_1_message_and_client_normal_close() throws InterruptedException {
        int servicePort1 = 9001;
        TCPService tcpService1 = new TCPService(servicePort1);

        tcpService1.startConnectionListener();

        Thread clientThread = new Thread(() -> {
            Socket socket = new Socket();
            OutputStream outputStream = null;
            try {
                socket.connect(new InetSocketAddress("127.0.0.1", servicePort1));
                outputStream = socket.getOutputStream();
                outputStream.write(("test").getBytes());
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    Objects.requireNonNull(outputStream).close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        clientThread.start();

        Thread.sleep(100);
        tcpService1.stopConnectionListener();
        assertThat(tcpService1.getPartnerSocket().size()).isEqualTo(0);
        /*tcpService1.getPartnerSocket().forEach(socket -> {
            ByteBuffer byteBufferOfSocket = tcpService1.getByteBufferOfSocket(socket);
            byteBufferOfSocket.flip();
            byte[] bytes = new byte[byteBufferOfSocket.limit()];
            byteBufferOfSocket.get(bytes);

            assertThat(new String(bytes)).isEqualTo("test");
        });*/
    }

    @RepeatedTest(100)
    void should_1_message_when_startMessageListener_given_send_1_message_and_client_not_close() throws InterruptedException {
        int servicePort1 = 9001;
        TCPService tcpService1 = new TCPService(servicePort1);
        tcpService1.startConnectionListener();
        Thread.sleep(100);
        Socket socket = new Socket();
        OutputStream outputStream;
        try {
            socket.connect(new InetSocketAddress("127.0.0.1", servicePort1));
            outputStream = socket.getOutputStream();
            outputStream.write(("test").getBytes());
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread.sleep(100);
        tcpService1.stopConnectionListener();

        assertThat(tcpService1.getPartnerSocket().size()).isEqualTo(1);
        tcpService1.getPartnerSocket().forEach(soc -> {
            ByteBuffer byteBufferOfSocket = tcpService1.getByteBufferOfSocket(soc);
            byteBufferOfSocket.flip();
            byte[] bytes = new byte[byteBufferOfSocket.limit()];
            byteBufferOfSocket.get(bytes);
            assertThat(new String(bytes)).isEqualTo("test");
        });
    }

    @RepeatedTest(100)
    void should_0_size_of_PartnerSocket_when_stopMessageListener() throws InterruptedException {
        int servicePort1 = 9011;
        TCPService tcpService1 = new TCPService(servicePort1);
        tcpService1.startConnectionListener();
        Thread.sleep(100);
        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress("127.0.0.1", servicePort1));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread.sleep(100);
        tcpService1.stopConnectionListener();
        tcpService1.stopMessageListener();
        Thread.sleep(100);
        Thread clientThread = new Thread(() -> {
            OutputStream outputStream;
            try {
                outputStream = socket.getOutputStream();
                outputStream.write(("test").getBytes());
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        clientThread.start();
        Thread.sleep(100);

        assertThat(tcpService1.getPartnerSocket().size()).isEqualTo(0);
    }

    @RepeatedTest(100)
    @Disabled
    void should_service_1_receive_1_size_message_when_sendMessage_given_service_1_and_service_2() throws InterruptedException {
        int servicePort1 = 9001;
        int servicePort2 = 9002;
        TCPService tcpService1 = new TCPService(servicePort1);
        tcpService1.startConnectionListener();
        Thread.sleep(50);
        TCPService tcpService2 = new TCPService(servicePort2);

        tcpService2.sendMessage(new InetSocketAddress("127.0.0.1", servicePort1), "test", null);
        Thread.sleep(50);

        tcpService1.stopConnectionListener();
        tcpService2.stopConnectionListener();
        assertThat(tcpService1.getPartnerSocket().size()).isEqualTo(1);
        tcpService1.getPartnerSocket().forEach(socket -> {
            ByteBuffer byteBufferOfSocket = tcpService1.getByteBufferOfSocket(socket);
            byteBufferOfSocket.flip();
            byte[] bytes = new byte[byteBufferOfSocket.limit()];
            byteBufferOfSocket.get(bytes);
            assertThat(new String(bytes)).isEqualTo("test");
        });

    }

    @RepeatedTest(100)
    void should_ConnectException_when_sendMessage_given_service_1_offline_and_service_2_online() throws InterruptedException {
        int servicePort1 = 9001;
        int servicePort2 = 9002;
        TCPService tcpService1 = new TCPService(servicePort1);
        tcpService1.startConnectionListener();
        Thread.sleep(50);
        tcpService1.stopConnectionListener();
        TCPService tcpService2 = new TCPService(servicePort2);

        assertThatExceptionOfType(ConnectException.class).isThrownBy(() -> tcpService2.sendMessage(new InetSocketAddress("127.0.0.1", servicePort1), "test", tcpService1));

        tcpService2.stopConnectionListener();
    }

    @RepeatedTest(100)
    void should_IOException_when_sendMessage_given_c1_connect_c2_then_c2_stop_messageListener() throws InterruptedException {
        int servicePort1 = 9001;
        int servicePort2 = 9011;
        TCPService tcpService1 = new TCPService(servicePort1);
        tcpService1.startConnectionListener();
        Thread.sleep(50);
        TCPService tcpService2 = new TCPService(servicePort2);
        tcpService2.startConnectionListener();
        Thread.sleep(50);

        assertThatExceptionOfType(IOException.class).isThrownBy(() -> {
            try {
                tcpService2.sendMessage(new InetSocketAddress("127.0.0.1", servicePort1), "test", tcpService1);
                Thread.sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            } finally {
                tcpService1.stopConnectionListener();
                tcpService1.stopMessageListener();
                tcpService2.stopMessageListener();
                tcpService2.stopConnectionListener();
            }
        });


    }

}
