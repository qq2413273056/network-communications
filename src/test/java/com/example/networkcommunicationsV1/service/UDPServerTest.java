package com.example.networkcommunicationsV1.service;

import com.example.networkcommunicationsV1.entity.ClientTcpInfo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

class UDPServerTest {

    private static UDPService udpService;

    @BeforeEach
    void load() {
        udpService = new UDPService();
    }

    @AfterEach
    void deleteData() {
        udpService.stopMessageListener();
        udpService.getMessageQueue().clear();
    }

    @RepeatedTest(100)
    void should_1_size_when_send_broadcast_message_given_exist_des_port() throws InterruptedException {
        int testCount = 1;
        int exist_des_port = 9999;
        udpService.openReceiveMessage(9999, () -> {

        });
        Thread.sleep(100);
        IntStream.rangeClosed(1, testCount).parallel().forEach((x) -> {
            ClientTcpInfo clientTcpInfo = ClientTcpInfo.builder().ip("1.1.1.1").port(22).build();
            udpService.sendBroadcastMessage(clientTcpInfo, exist_des_port);
        });


        Thread.sleep(100);
        assertThat(udpService.getMessageQueue().size()).isEqualTo(1);
    }

    @RepeatedTest(100)
    void should_1_size_when_send_repeat_broadcast_message_given_exist_des_port() throws InterruptedException {
        int testCount = 10;
        int exist_des_port = 9999;
        udpService.openReceiveMessage(9999, () -> {

        });
        Thread.sleep(100);
        IntStream.rangeClosed(1, testCount).parallel().forEach((x) -> {
            ClientTcpInfo clientTcpInfo = ClientTcpInfo.builder().ip("1.1.1.1").port(22).build();
            udpService.sendBroadcastMessage(clientTcpInfo, exist_des_port);
        });


        Thread.sleep(100);
        assertThat(udpService.getMessageQueue().size()).isEqualTo(1);
    }


    @RepeatedTest(100)
    void should_0_size_when_send_broadcast_message_given_not_exist_des_port() throws InterruptedException {
        int testCount = 10;
        int not_exist_des_port = 9998;
        udpService.openReceiveMessage(9999, () -> {

        });
        Thread.sleep(100);

        IntStream.rangeClosed(1, testCount).parallel().forEach((x) -> {
            ClientTcpInfo clientTcpInfo = ClientTcpInfo.builder().ip("1.1.1.1").port(22).build();
            udpService.sendBroadcastMessage(clientTcpInfo, not_exist_des_port);
        });

        Thread.sleep(100);
        assertThat(udpService.getMessageQueue().size()).isEqualTo(0);
    }

    @RepeatedTest(100)
    void should_0_size_when_send_broadcast_message_given_not_open_receive_message() throws InterruptedException {
        int testCount = 100;
        int not_listener_port = 9999;

        IntStream.rangeClosed(1, testCount).parallel().forEach((x) -> {
            ClientTcpInfo clientTcpInfo = ClientTcpInfo.builder().ip("1.1.1.1").port(22).build();
            udpService.sendBroadcastMessage(clientTcpInfo, not_listener_port);
        });

        Thread.sleep(100);
        assertThat(udpService.getMessageQueue().size()).isEqualTo(0);
    }

    @RepeatedTest(100)
    void should_0_size_when_send_broadcast_message_given_stopped_listener() throws InterruptedException {
        int testCount = 100;
        int exist_des_port = 9999;
        udpService.stopMessageListener();

        IntStream.rangeClosed(1, testCount).parallel().forEach((x) -> {
            ClientTcpInfo clientTcpInfo = ClientTcpInfo.builder().ip("1.1.1.1").port(22).build();
            udpService.sendBroadcastMessage(clientTcpInfo, exist_des_port);
        });

        Thread.sleep(100);
        assertThat(udpService.getMessageQueue().size()).isEqualTo(0);
    }

    @RepeatedTest(100)
    void should_1_size_when_send_single_message_given_exist_des_port() throws InterruptedException {
        int exist_des_port = 9999;
        udpService.openReceiveMessage(9999, () -> {
        });
        ClientTcpInfo resMessage = ClientTcpInfo.builder().ip("127.0.0.1").port(23).build();

        udpService.sendMessage(resMessage, "127.0.0.1", exist_des_port);

        Thread.sleep(100);
        assertThat(udpService.getMessageQueue().size()).isEqualTo(1);
    }

    @RepeatedTest(100)
    void should_reply_message_when_receive_message_given_one_message() throws InterruptedException {
        int testCount = 1;
        int exist_des_port = 9999;
        udpService.openReceiveMessage(9999, () -> {
            ClientTcpInfo resMessage = ClientTcpInfo.builder().ip("127.0.0.1").port(23).build();
            udpService.sendMessage(resMessage, "127.0.0.1", exist_des_port);
        });

        Thread.sleep(100);
        IntStream.rangeClosed(1, testCount).forEach((x) -> {
            ClientTcpInfo clientTcpInfo = ClientTcpInfo.builder().ip("127.0.0.1").port(22).build();
            udpService.sendBroadcastMessage(clientTcpInfo, exist_des_port);
        });


        Thread.sleep(100);
        assertThat(udpService.getMessageQueue().size()).isEqualTo(2);
    }

}
