package com.example.networkcommunicationsV1.cs;

import com.example.networkcommunicationsV1.entity.TCPMessage;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.net.ConnectException;
import java.net.SocketException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ClientTest {

    @RepeatedTest(100)
    void should_success_when_start_given_exist_des_port() throws InterruptedException {
        int unused_port = 10001;
        Server server = new Server(unused_port);
        server.startConnectionListener();

        Client client = new Client("192.168.1.101", unused_port);
        client.start();
        Thread.sleep(100);
        assertThat(server.getClientCount()).isEqualTo(1);
        server.stopConnectionListener();
    }

    @RepeatedTest(100)
    void should_ConnectException_when_start_given_not_exist_des_port() {
        Client client = new Client("192.168.1.101", 37893);
        assertThatExceptionOfType(ConnectException.class).isThrownBy(client::start);
    }

    @Test
    void should_success_when_shutdown() {
        int desPort = 10001;
        Client client = new Client("192.168.1.101", desPort);
        client.shutdown();
    }


    @RepeatedTest(100)
    void should_SocketException_when_sendMessage_given_server_close_messageListener() throws InterruptedException {
        int unused_port = 10001;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Client client = new Client("192.168.1.101", unused_port);
        client.start();
        Thread.sleep(100);
        server.stopMessageListener();
        Thread.sleep(100);
        TCPMessage tcpMessage = TCPMessage.builder().id(UUID.randomUUID().toString()).to(null).from(client.getLocalSocketAddress()).data("test").build();

        assertThatExceptionOfType(SocketException.class).isThrownBy(() -> {
            try {
                client.sendMessage(tcpMessage);
            } finally {
                server.stopConnectionListener();
            }
        });
    }

    @RepeatedTest(1)
    void should_success_when_sendMessage_given_exist_des_client() throws InterruptedException {
        int unused_port = 10001;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();

        Client client = new Client("192.168.1.101", desPort);
        client.start();

        Client client2 = new Client("192.168.1.101", desPort);
        client2.start();

        TCPMessage tcpMessage = TCPMessage.builder().id(UUID.randomUUID().toString()).to(client2.getLocalSocketAddress()).from(client.getLocalSocketAddress()).data("test").build();
        client.sendMessage(tcpMessage);
        Thread.sleep(300);

        assertThat(client.getMessageCountOfSending()).isEqualTo(0);
        server.stopConnectionListener();
        server.stopMessageListener();
    }

    @RepeatedTest(100)
    void should_TargetNotExist_when_sendMessage_given_not_exist_des_client() throws InterruptedException {
        int unused_port = 10001;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Client client = new Client("192.168.1.101", desPort);
        client.start();


        TCPMessage tcpMessage = TCPMessage.builder().id(UUID.randomUUID().toString()).to("not_exist_des_client").from(client.getLocalSocketAddress()).data("test").build();
        client.sendMessage(tcpMessage);
        Thread.sleep(300);

        assertThat(client.getMessageCountOfTargetOffline()).isEqualTo(1);
        server.stopConnectionListener();
        server.stopMessageListener();
    }

    @RepeatedTest(100)
    void should_TargetOffline_when_sendMessage_given_offline_des_client() throws InterruptedException {
        int unused_port = 10001;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Client client = new Client("192.168.1.101", desPort);
        client.start();

        Client client2 = new Client("192.168.1.101", desPort);
        client2.start();
        Thread.sleep(100);

        TCPMessage tcpMessage = TCPMessage.builder().id(UUID.randomUUID().toString()).to(client2.getLocalSocketAddress()).from(client.getLocalSocketAddress()).data("test").build();
        client2.shutdown();

        client.sendMessage(tcpMessage);
        Thread.sleep(300);

        server.stopConnectionListener();
        server.stopMessageListener();
        assertThat(client.getMessageCountOfTargetOffline()).isEqualTo(1);
    }

    @RepeatedTest(100)
    void should_1_size_when_sendMessage_given_null_des_client_and_one_client_offline() throws InterruptedException {
        int unused_port = 10001;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Client client = new Client("192.168.1.101", desPort);
        client.start();

        Client client2 = new Client("192.168.1.101", desPort);
        client2.start();
        Thread.sleep(100);

        Client client3 = new Client("192.168.1.101", desPort);
        client3.start();
        Thread.sleep(100);
        client3.shutdown();
        Thread.sleep(100);

        TCPMessage tcpMessage = TCPMessage.builder().id(UUID.randomUUID().toString()).to(null).from(client.getLocalSocketAddress()).data("test").build();
        client.sendMessage(tcpMessage);
        Thread.sleep(300);

        server.stopConnectionListener();
        server.stopMessageListener();
        assertThat(client.getMessageCountOfSending()).isEqualTo(0);
    }

}
