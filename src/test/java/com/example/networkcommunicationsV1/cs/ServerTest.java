package com.example.networkcommunicationsV1.cs;

import com.alibaba.fastjson.JSON;
import com.example.networkcommunicationsV1.codec.Encode;
import com.example.networkcommunicationsV1.entity.TCPMessage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.UUID;

import static com.example.networkcommunicationsV1.entity.TCPMessage.MessageType.TARGET_OFFLINE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ServerTest {

    @Test
        //什么时候删除测试 //测试归类
    void should_success_when_startConnectionListener_given_unused_port() {
        int unused_port = 10001;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        server.stopConnectionListener();
    }

    @Test
    void should_BindException_when_startConnectionListener_given_used_port() {
        int unused_port = 10001;
        Server server = new Server(unused_port);
        server.startConnectionListener();

        assertThatExceptionOfType(BindException.class).isThrownBy(() -> {
            try {
                server.startConnectionListener();
            } finally {
                server.stopConnectionListener();//抽取after  //bdd断言
            }
        });
    }

    @RepeatedTest(100)
//repeat test 什么时候用 //没有断言的测试
    void should_success_when_stopConnectionListener_given_started_server() {
        int unused_port = 10001;
        Server server = new Server(unused_port);
        server.startConnectionListener();

        server.stopConnectionListener();
    }

    @RepeatedTest(100)
    void should_success_when_stopConnectionListener_given_not_start_server() {
        int unused_port = 10001;
        Server server = new Server(unused_port);

        server.stopConnectionListener();
    }

    @RepeatedTest(100)
    void should_1_size_when_startMessageListener_given_one_client_send_message() throws InterruptedException, IOException {
        int unused_port = 10021;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Thread.sleep(100);

        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("192.168.1.101", desPort));
            TCPMessage tcpMessage = TCPMessage.builder().to(socket.getLocalSocketAddress().toString()).data("test").build();
            socket.getOutputStream().write(Encode.encode(tcpMessage));
            socket.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

        Thread.sleep(100);
        server.stopConnectionListener();// 提取 f3  command+2
        server.stopMessageListener();//
        assertThat(server.getMessageQueue().size()).isEqualTo(1);
    }

    @RepeatedTest(100)
    void should_ConnectException_when_stopConnectionListener_given_one_client_to_connect() {
        int unused_port = 10001;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();

        server.stopConnectionListener();

        assertThatExceptionOfType(ConnectException.class).isThrownBy(() -> {
            Client client = new Client("127.0.0.1", desPort);
            client.start();
        });
    }

    @RepeatedTest(100)
    void should_delete_info_when_client_to_disconnect() throws InterruptedException {
        int unused_port = 10001;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Thread.sleep(100);

        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("127.0.0.1", desPort));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread.sleep(100);
        assertThat(server.getClientCount()).isEqualTo(0);
        server.stopConnectionListener();
    }


    @RepeatedTest(100)
    void should_0_when_stopMessageListener_given_one_client_send_message() throws InterruptedException, IOException {
        int unused_port = 10021;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Thread.sleep(100);

        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("127.0.0.1", desPort));
            Thread.sleep(50);
            server.stopMessageListener();
            socket.getOutputStream().write("test\n".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

        Thread.sleep(100);
        server.stopConnectionListener();
        assertThat(server.getMessageQueue().size()).isEqualTo(0);
    }


    @RepeatedTest(100)
    void should_1_size_when_sendMessage_given_client_receive_message_and_message_have_des_address() throws InterruptedException, IOException {
        int unused_port = 10021;
        int desPort = unused_port;

        Server server = new Server(unused_port);
        server.startConnectionListener();
        try (Socket socket = new Socket()) {
            Thread.sleep(100);
            socket.connect(new InetSocketAddress("192.168.1.101", desPort));
            Thread.sleep(100);
            TCPMessage tcpMessage = TCPMessage.builder().to(socket.getLocalSocketAddress().toString()).data("test").build();

            server.sendMessage(tcpMessage);
            Thread.sleep(100);
            byte[] results = new byte[1024];
            int read = socket.getInputStream().read(results);
            String s = new String(results, 0, read);
            assertThat(s).isEqualTo(JSON.toJSONString(tcpMessage) + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            server.stopConnectionListener();
            server.stopMessageListener();
        }
    }

    @RepeatedTest(100)
    void should_1_size_when_sendMessage_given_client_receive_message_and_empty_des_address() throws InterruptedException, IOException {
        int unused_port = 10021;
        int desPort = unused_port;

        Server server = new Server(unused_port);
        server.startConnectionListener();

        try (Socket socket = new Socket()) {
            Thread.sleep(100);
            socket.connect(new InetSocketAddress("192.168.1.101", desPort));
            Thread.sleep(100);
            TCPMessage tcpMessage = TCPMessage.builder().to(null).data("test").build();

            server.sendMessage(tcpMessage);
            Thread.sleep(100);
            byte[] results = new byte[1024];
            int read = socket.getInputStream().read(results);
            String s = new String(results, 0, read);
            assertThat(s).isEqualTo(JSON.toJSONString(tcpMessage) + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            server.stopConnectionListener();
            server.stopMessageListener();
        }
    }

    @RepeatedTest(100)
    void should_TARGET_OFFLINE_of_messageType_when_sendMessage_given_client_receive_message_and_not_exist_des_address() throws InterruptedException, IOException {
        int unused_port = 10021;
        int desPort = unused_port;

        Server server = new Server(unused_port);
        server.startConnectionListener();
        try (Socket socket = new Socket()) {
            Thread.sleep(100);
            socket.connect(new InetSocketAddress("192.168.1.101", desPort));
            Thread.sleep(100);
            TCPMessage tcpMessage = TCPMessage.builder().to("not_exist_des_address").data("test").build();

            server.sendMessage(tcpMessage);
            assertThat(tcpMessage.getMessageType()).isEqualTo(TARGET_OFFLINE);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            server.stopConnectionListener();
            server.stopMessageListener();
        }
    }

    @RepeatedTest(100)
    @DisplayName("客户端正常关闭")
    void should_TARGET_OFFLINE_of_messageType_when_sendMessage_given_client_stop_receive_message() throws InterruptedException {
        int unused_port = 10021;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Socket socket = new Socket();
        try {
            Thread.sleep(100);
            socket.connect(new InetSocketAddress("192.168.1.101", desPort));
            TCPMessage tcpMessage = TCPMessage.builder().id(UUID.randomUUID().toString()).from("1.1.1.1").to(socket.getLocalSocketAddress().toString()).data("test").build();
            Thread.sleep(100);
            socket.close();
            Thread.sleep(100);

            server.sendMessageNeedAck(tcpMessage);
            assertThat(tcpMessage.getMessageType()).isEqualTo(TARGET_OFFLINE);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            server.stopConnectionListener();
            server.stopMessageListener();
        }
    }

    @RepeatedTest(1)
    @DisplayName("客户端网络断开")
    void should_success_when_sendMessage_given_client_error() throws InterruptedException {
        int unused_port = 10021;
        int desPort = unused_port;
        Server server = new Server(unused_port);
        server.startConnectionListener();
        Socket socket = new Socket();
        try {
            Thread.sleep(100);
            socket.connect(new InetSocketAddress("192.168.1.101", desPort));
            TCPMessage tcpMessage = TCPMessage.builder().to(socket.getLocalSocketAddress().toString()).data("test").build();
            Thread.sleep(100);
            //TODO 断网模拟
            server.sendMessage(tcpMessage);
            //TODO 网络恢复
            Thread.sleep(1000);
            byte[] results = new byte[1024];
            int read = socket.getInputStream().read(results);
            String s = new String(results, 0, read);
            assertThat(s).isEqualTo(JSON.toJSONString(tcpMessage) + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            server.stopConnectionListener();
            server.stopMessageListener();
        }
    }

    @RepeatedTest(100)
    void should_0_size_of_sendingCount_when_sendMessageNeedAck_given_client_reply_ack() throws InterruptedException, IOException {
        int unused_port = 10021;
        int desPort = unused_port;

        Server server = new Server(unused_port);
        server.startConnectionListener();
        try (Socket socket = new Socket()) {
            Thread.sleep(100);
            socket.connect(new InetSocketAddress("192.168.1.101", desPort));
            Thread.sleep(100);
            TCPMessage tcpMessage = TCPMessage.builder().id(UUID.randomUUID().toString()).to(socket.getLocalSocketAddress().toString()).data("test").build();

            server.sendMessageNeedAck(tcpMessage);
            assertThat(server.getSendingMessageCount()).isEqualTo(1);
            Thread.sleep(100);
            byte[] results = new byte[1024];
            int read = socket.getInputStream().read(results);
            String s = new String(results, 0, read);
            assertThat(s).isEqualTo(JSON.toJSONString(tcpMessage) + "\n");
            socket.getOutputStream().write(Encode.encode(TCPMessage.builder().id(tcpMessage.getId()).messageType(TCPMessage.MessageType.ACK).build()));
            Thread.sleep(100);
            assertThat(server.getSendingMessageCount()).isEqualTo(0);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            server.stopConnectionListener();
            server.stopMessageListener();
        }
    }

    @RepeatedTest(100)
    void should_1_size_of_sendingCount_when_sendMessageNeedAck_given_client_not_reply_ack() throws InterruptedException, IOException {
        int unused_port = 10021;
        int desPort = unused_port;

        Server server = new Server(unused_port);
        server.startConnectionListener();
        try (Socket socket = new Socket()) {
            Thread.sleep(100);
            socket.connect(new InetSocketAddress("192.168.1.101", desPort));
            Thread.sleep(100);
            TCPMessage tcpMessage = TCPMessage.builder().id(UUID.randomUUID().toString()).to(socket.getLocalSocketAddress().toString()).data("test").build();

            server.sendMessageNeedAck(tcpMessage);
            assertThat(server.getSendingMessageCount()).isEqualTo(1);
            Thread.sleep(100);
            byte[] results = new byte[1024];
            int read = socket.getInputStream().read(results);
            String s = new String(results, 0, read);
            assertThat(s).isEqualTo(JSON.toJSONString(tcpMessage) + "\n");

            assertThat(server.getSendingMessageCount()).isEqualTo(1);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            server.stopConnectionListener();
            server.stopMessageListener();
        }
    }


}
