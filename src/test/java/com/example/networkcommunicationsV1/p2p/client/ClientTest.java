package com.example.networkcommunicationsV1.p2p.client;

import com.example.networkcommunicationsV1.exception.PartnerSocketCloseException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ClientTest {

    @RepeatedTest(100)
    void should_0_size_when_start_given_one_client() throws InterruptedException {
        int udpListenerPort = 9999;
        int udpDesPort = 9997;
        int tcpListenerPort = 9998;
        Client client = new Client(udpListenerPort, udpDesPort, tcpListenerPort);
        client.start();

        Thread.sleep(100);
        client.shutdown();
        assertThat(client.getPartnerTcpInfo().size()).isEqualTo(0);
    }

    @RepeatedTest(100)
    void should_exception_when_repeat_start_given_one_client() throws InterruptedException {
        int udpListenerPort = 9999;
        int tcpListenerPort = 9998;
        Client client = new Client(udpListenerPort, 10000, tcpListenerPort);
        client.start();
        Thread.sleep(100);

        assertThatExceptionOfType(BindException.class).isThrownBy(() -> {
            try {
                client.start();
            } finally {
                client.shutdown();
            }
        });
    }

    @RepeatedTest(100)
    void should_discover_each_other_when_start_given_two_client() throws InterruptedException {
        int udpListenerPort = 9999;
        int udpDesPort = 9997;
        int tcpListenerPort = 9998;
        Client client = new Client(udpListenerPort, udpDesPort, tcpListenerPort);
        client.start();

        Client client2 = new Client(udpDesPort, udpListenerPort, tcpListenerPort + 10);
        client2.start();

        Thread.sleep(100);
        client.shutdown();
        client2.shutdown();

        assertThat(client.getPartnerTcpInfo().size()).isEqualTo(1);
        assertThat(client2.getPartnerTcpInfo().size()).isEqualTo(1);
    }


    @Test
    void should_no_exception_when_shutdown_given_one_client() {
        int udpListenerPort = 9999;
        int tcpListenerPort = 9998;
        Client client = new Client(udpListenerPort, udpListenerPort, tcpListenerPort);
        client.start();

        client.shutdown();
    }


    @RepeatedTest(100)
    void should_1_size_when_send_message_given_two_client() throws InterruptedException {
        int udpListenerPort = 9999;
        int udpDesPort = 9997;
        int tcpListenerPort = 9998;
        Client client = new Client(udpListenerPort, udpDesPort, tcpListenerPort);
        client.start();

        Client client2 = new Client(udpDesPort, udpListenerPort, tcpListenerPort + 10);
        client2.start();

        Thread.sleep(100);

        client.sendMessage(client.getPartnerTcpInfo().get(0), "hello\n");
        client2.sendMessage(client2.getPartnerTcpInfo().get(0), "hello\n");
        Thread.sleep(100);
        client.shutdown();
        client2.shutdown();
        assertThat(client.getTcpMessageCount()).isEqualTo(1);
        assertThat(client2.getTcpMessageCount()).isEqualTo(1);
    }

    @RepeatedTest(100)
    @DisplayName("单发消息，被连接方关闭连接监听")
    void should_PartnerSocketCloseException_when_send_message_given_one_client_and_one_client_shutdown() throws InterruptedException {
        int udpListenerPort = 9999;
        int udpDesPort = 9997;
        int tcpListenerPort = 9998;
        Client client = new Client(udpListenerPort, udpDesPort, tcpListenerPort);
        client.start();

        Client client2 = new Client(udpDesPort, udpListenerPort, tcpListenerPort + 10);
        client2.start();

        Thread.sleep(100);
        client.shutdown();
        Thread.sleep(100);

        assertThatExceptionOfType(PartnerSocketCloseException.class).isThrownBy(() -> {
            try {
                client2.sendMessage(client2.getPartnerTcpInfo().get(0), "hello\n");
            } finally {
                client2.shutdown();
            }
        });

    }

    @RepeatedTest(100)
    void should_PartnerSocketCloseException_when_send_single_message_given_one_client_and_one_client_stopMessageListener() throws InterruptedException {
        int udpListenerPort = 9999;
        int udpDesPort = 9997;
        int tcpListenerPort = 10001;
        Client client = new Client(udpListenerPort, udpDesPort, tcpListenerPort);
        client.start();

        Client client2 = new Client(udpDesPort, udpListenerPort, tcpListenerPort + 10);
        client2.start();

        Thread.sleep(100);

        assertThatExceptionOfType(PartnerSocketCloseException.class).isThrownBy(() -> {
            try {
                Socket socket = client2.getPartnerSocket(client2.getPartnerTcpInfo().get(0));
                Thread.sleep(100);
                client.stopMessageListener();
                client2.sendMessage(socket, "hello\n");
            } finally {
                client.shutdown();
                client2.shutdown();
            }
        });


    }

    @RepeatedTest(100)
    void should_100_size_when_send_100_single_message_given_two_client() throws InterruptedException {
        int udpListenerPort = 9999;
        int udpDesPort = 9997;
        int tcpListenerPort = 9998;
        Client client = new Client(udpListenerPort, udpDesPort, tcpListenerPort);
        client.start();

        Client client2 = new Client(udpDesPort, udpListenerPort, tcpListenerPort + 10);
        client2.start();

        Thread.sleep(100);

        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(client.getPartnerTcpInfo().get(0).getIp(), client.getPartnerTcpInfo().get(0).getPort()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        IntStream.rangeClosed(1, 100).forEach(value -> {
            try {
                socket.getOutputStream().write("hello".getBytes());
                socket.getOutputStream().write("\n".getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Thread.sleep(100);
        client.shutdown();
        client2.shutdown();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(client2.getTcpMessageCount()).isEqualTo(100);
    }

    @RepeatedTest(100)
    void should_1_size_when_send_mass_message_given_two_client() throws InterruptedException {
        int udpListenerPort = 9999;
        int udpDesPort = 9997;
        int tcpListenerPort = 9998;
        Client client = new Client(udpListenerPort, udpDesPort, tcpListenerPort);
        client.start();
        Client client2 = new Client(udpDesPort, udpListenerPort, tcpListenerPort + 10);
        client2.start();
        Thread.sleep(100);

        client.sendMessage("hello\n");

        Thread.sleep(100);
        client.shutdown();
        client2.shutdown();
        assertThat(client2.getTcpMessageCount()).isEqualTo(1);
    }
}
