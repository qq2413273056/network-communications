package com.example.networkcommunicationsV1.codec;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class DecodeTest {
    @RepeatedTest(100)
    void should_success_when_decode_given_bytes() {
        byte[] bytes = "test\n".getBytes();
        Decode decode = new Decode();
        ByteBuffer wrap = ByteBuffer.allocate(1024);
        List<Object> results = Lists.list();
        for (byte aByte : bytes) {
            wrap.put(aByte);
            String decode1 = decode.decode(wrap);
            if (decode1 != null) {
                assertThat(decode1).isEqualTo("test");
                results.add(decode1);
            }
        }
        assertThat(results.size()).isEqualTo(1);
    }

    @Test
    void should_success_when_repeat_decode_given_bytes() {
        byte[] bytes = "test\ntest\ntest\ntest\n".getBytes();
        Decode decode = new Decode();
        ByteBuffer wrap = ByteBuffer.allocate(1024);
        List<Object> results = Lists.list();
        for (byte aByte : bytes) {
            wrap.put(aByte);
            String decode1 = decode.decode(wrap);
            if (decode1 != null) {
                assertThat(decode1).isEqualTo("test");
                results.add(decode1);
            }
        }
        assertThat(results.size()).isEqualTo(4);
    }

    @Test
    void should_3_when_repeat_decode_given_bytes() {
        byte[] bytes = "test\ntest\ntest\ntest".getBytes();
        Decode decode = new Decode();
        ByteBuffer wrap = ByteBuffer.allocate(1024);
        List<Object> results = Lists.list();
        for (byte aByte : bytes) {
            wrap.put(aByte);
            String decode1 = decode.decode(wrap);
            if (decode1 != null) {
                assertThat(decode1).isEqualTo("test");
                results.add(decode1);
            }
        }
        assertThat(results.size()).isEqualTo(3);
    }

    @Test
    void should_0_when_decode_given_bytes_error() {
        byte[] bytes = "test".getBytes();
        Decode decode = new Decode();
        ByteBuffer wrap = ByteBuffer.allocate(1024);
        List<Object> results = Lists.list();
        for (byte aByte : bytes) {
            wrap.put(aByte);
            String decode1 = decode.decode(wrap);
            if (decode1 != null) {
                assertThat(decode1).isEqualTo("test");
                results.add(decode1);
            }
        }
        assertThat(results.size()).isEqualTo(0);
    }

    @Test
    void should_0_when_decode_given_bytes_0() {
        byte[] bytes = "".getBytes();
        Decode decode = new Decode();
        ByteBuffer wrap = ByteBuffer.allocate(1024);
        List<Object> results = Lists.list();

        for (byte aByte : bytes) {
            wrap.put(aByte);
            String decode1 = decode.decode(wrap);
            if (decode1 != null) {
                assertThat(decode1).isEqualTo("test");
                results.add(decode1);
            }
        }
        assertThat(results.size()).isEqualTo(0);
    }

}
