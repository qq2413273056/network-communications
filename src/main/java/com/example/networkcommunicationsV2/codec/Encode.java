package com.example.networkcommunicationsV2.codec;

import com.alibaba.fastjson.JSON;
import lombok.Builder;
import lombok.NonNull;

@Builder
public class Encode {
    public static byte[] encode(@NonNull Object object) {
        String json = JSON.toJSONString(object) + "\n";
        return json.getBytes();
    }
}
