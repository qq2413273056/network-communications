package com.example.networkcommunicationsV2.entity;

import com.alibaba.fastjson.JSON;
import lombok.*;

import java.util.UUID;

import static com.example.networkcommunicationsV2.entity.ClientTcpInfo.MessageType.BROADCAST;
import static com.example.networkcommunicationsV2.entity.ClientTcpInfo.MessageType.REPLY;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class ClientTcpInfo {
    private String id;
    private String ip;
    private Integer port;
    private MessageType messageType;

    public void transformMessageTypeForReply() {
        this.messageType = REPLY;
    }

    public void checkAndFill4SendBroadcast() {
        this.messageType = BROADCAST;
        if (id == null) {
            id = UUID.randomUUID().toString();
        }
    }

    public enum MessageType {
        BROADCAST,
        REPLY
    }

    public String generateKey() {
        return ip + "|" + port;
    }

    public byte[] toJsonBytes() {
        return JSON.toJSONString(this).getBytes();
    }

}
