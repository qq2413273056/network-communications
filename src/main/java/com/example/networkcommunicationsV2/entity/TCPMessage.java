package com.example.networkcommunicationsV2.entity;

import lombok.*;

@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
public class TCPMessage {
    private String id;
    private String from;
    private String to;
    private String data;
    private MessageType messageType;
    private Integer needAckCount;
    private Integer ackCount;
    private String description;


    public enum MessageType {
        NORMAL,
        ACK,
        TARGET_NOT_EXIST,
        TARGET_OFFLINE,
    }
}

