package com.example.networkcommunicationsV2.p2p;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.example.networkcommunicationsV2.codec.Decode;
import com.example.networkcommunicationsV2.entity.ClientTcpInfo;
import com.example.networkcommunicationsV2.entity.TCPMessage;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import static com.example.networkcommunicationsV2.entity.ClientTcpInfo.MessageType.BROADCAST;
import static com.example.networkcommunicationsV2.entity.ClientTcpInfo.MessageType.REPLY;
import static com.example.networkcommunicationsV2.entity.TCPMessage.MessageType.*;


public class Client {
    private static final String BROADCAST_ADDR = "255.255.255.255";
    private static final int DESTINATION_BROADCAST_PORT = 9999;

    private ServerSocket serverSocket;
    private DatagramSocket datagramSocket;

    private final ConcurrentHashMap<String, ClientTcpInfo> udpSendingMap = new ConcurrentHashMap<>();
    private final LinkedBlockingQueue<TCPMessage> tcpReceiveMessageQueue = new LinkedBlockingQueue<>();

    private final ConcurrentHashMap<String, ClientTcpInfo> partnerTcpInfoMap = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, TCPMessage> tcpSendingMap = new ConcurrentHashMap<>();


    private ConcurrentHashMap<String, Socket> partnerSocketMapForWrite = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Socket> partnerSocketMap4Read = new ConcurrentHashMap<>();

    private ConcurrentHashMap<Socket, ByteBuffer> socket2ByteBuffer = new ConcurrentHashMap<>();

    private boolean isOkOfTCPConnectionListener;
    private boolean isOkOfUDPMessageListener;
    private Thread udpMessageListenerThread;
    private Thread tcpConnectionListenerThread;


    public Client() {
        try {
            serverSocket = new ServerSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @SneakyThrows
    public void sendBroadcastMessage(@NonNull ClientTcpInfo clientTcpInfo) {
        clientTcpInfo.checkAndFill4SendBroadcast();

        doSendUdpMessage(BROADCAST_ADDR, DESTINATION_BROADCAST_PORT, clientTcpInfo);

        postSendUdpMessage(clientTcpInfo);
    }

    @SneakyThrows
    private void doSendUdpMessage(String ip, int port, ClientTcpInfo clientTcpInfo) {
        byte[] clientTcpInfoBytes = clientTcpInfo.toJsonBytes();
        InetSocketAddress inetSocketAddress = new InetSocketAddress(ip, port);
        DatagramPacket datagramPacket = new DatagramPacket(clientTcpInfoBytes, clientTcpInfoBytes.length, inetSocketAddress);
        datagramSocket.send(datagramPacket);
    }

    private void postSendUdpMessage(ClientTcpInfo clientTcpInfo) {
        udpSendingMap.put(clientTcpInfo.getId(), clientTcpInfo);
    }

    @SneakyThrows
    private void sendUdpReply(InetSocketAddress address, ClientTcpInfo clientTcpInfo) {
        clientTcpInfo.transformMessageTypeForReply();

        doSendUdpMessage(address.getAddress().getHostAddress(), address.getPort(), clientTcpInfo);
    }


    @SneakyThrows
    public void startTCPConnectionListener(int tcpListenerPort) {
        if (!isCompleteOfTcpConnectionListener()) {
            preStartTcpConnectionListener(tcpListenerPort);

            doStartTcpConnectionListener();

            postStartTcpConnectionListener();
        }
    }

    private void preStartTcpConnectionListener(int tcpListenerPort) throws IOException {
        if (serverSocket == null) {
            serverSocket = new ServerSocket();
        }
        if (!serverSocket.isBound()) {
            serverSocket.bind(new InetSocketAddress(tcpListenerPort));
        }
    }

    private void doStartTcpConnectionListener() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        tcpConnectionListenerThread = new Thread(() -> {
            countDownLatch.countDown();
            while (!tcpConnectionListenerThread.isInterrupted() && !serverSocket.isClosed()) {
                try {
                    doTcpConnectionListener();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        tcpConnectionListenerThread.start();
        countDownLatch.await();
    }

    private void postStartTcpConnectionListener() {
        isOkOfTCPConnectionListener = true;
    }

    private void doTcpConnectionListener() throws IOException {
        Socket accept = serverSocket.accept();

        monitorTcpMessage(accept);
    }


    public void monitorTcpMessage(Socket accept) {
        initReadSocketData(accept);

        new Thread(() -> {
            while (!accept.isClosed()) {
                try {
                    InputStream inputStream = accept.getInputStream();
                    int read = inputStream.read();
                    if (read == -1) {
                        clearReadSocketDataAfterClose(accept);
                        return;
                    }
                    ByteBuffer byteBuffer = socket2ByteBuffer.get(accept);
                    if (byteBuffer == null) {
                        ByteBuffer allocate = ByteBuffer.allocate(1024);
                        socket2ByteBuffer.put(accept, allocate);
                        byteBuffer = allocate;
                    }
                    byteBuffer.put((byte) (read & 0xff));
                    String decode = new Decode().decode(byteBuffer);
                    if (!StringUtils.isEmpty(decode)) {
                        handleTcpMessage(decode);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    socket2ByteBuffer.get(accept).clear();
                } catch (IOException e) {
                    clearReadSocketDataAfterClose(accept);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void initReadSocketData(Socket accept) {
        partnerSocketMap4Read.put(getKeyBy(accept), accept);
        socket2ByteBuffer.put(accept, ByteBuffer.allocate(1024));
    }

    private void clearReadSocketDataAfterClose(Socket accept) {
        partnerSocketMap4Read.remove(getKeyBy(accept));
        socket2ByteBuffer.remove(accept);
        try {
            accept.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void handleTcpMessage(String decode) {
        TCPMessage message = JSON.parseObject(decode, TCPMessage.class);
        switch (message.getMessageType()) {
            case NORMAL:
                tcpReceiveMessageQueue.add(message);
                sendAckTcpMessage(message);
                break;
            case ACK:
                tcpSendingMap.remove(message.getId());
                break;
        }
    }

    @SneakyThrows
    public void startUDPMessageListener(int udpListenerPort) {
        if (!isCompleteOfStartUDPMessageListener()) {
            preStartUdpMessageListener(udpListenerPort);

            doStartUdpMessageListener();

            postStartUdpMessageListener();
        }
    }

    private void postStartUdpMessageListener() {
        isOkOfUDPMessageListener = true;
    }

    private void doStartUdpMessageListener() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        udpMessageListenerThread = new Thread(() -> {
            countDownLatch.countDown();
            while (!datagramSocket.isClosed() && !udpMessageListenerThread.isInterrupted()) {
                try {
                    byte[] bytes = new byte[1024];
                    DatagramPacket receiveDatagramPacket = new DatagramPacket(bytes, 1024);
                    datagramSocket.receive(receiveDatagramPacket);
                    if (receiveDatagramPacket.getPort() == -1) {
                        continue;
                    }
                    String jsonData = new String(receiveDatagramPacket.getData(), 0, receiveDatagramPacket.getData().length);
                    ClientTcpInfo clientTcpInfo = JSON.parseObject(jsonData, ClientTcpInfo.class);
                    InetSocketAddress socketAddress = (InetSocketAddress) receiveDatagramPacket.getSocketAddress();

                    handleUdpMessage(socketAddress, clientTcpInfo);
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
        udpMessageListenerThread.start();
        countDownLatch.await();
    }

    private void preStartUdpMessageListener(int udpListenerPort) throws SocketException {
        if (datagramSocket == null) {
            datagramSocket = new DatagramSocket(udpListenerPort);
        }
        if (!datagramSocket.isBound()) {
            datagramSocket.bind(new InetSocketAddress(udpListenerPort));
        }
    }

    private void handleUdpMessage(InetSocketAddress socketAddress, ClientTcpInfo clientTcpInfo) {
        if (isSendBySelf(clientTcpInfo) || isContainPartnerTcpInfo(clientTcpInfo)) {
            return;
        }

        createAndSaveConnectionBy(clientTcpInfo);
        if (clientTcpInfo.getMessageType() == BROADCAST) {
            sendUdpReply(socketAddress, generateLocalClientTcpInfo());
        }
    }


    @SneakyThrows
    public void sendTcpMessage(String ip, int port, String msg) {
        TCPMessage tcpMessage = generateTcpSendMessageBy(msg);
        String key = generateKey(ip, port);

        preSendTcpMessage(tcpMessage, key);
        Socket desSocket = partnerSocketMapForWrite.get(key);
        try {
            doSendTcpMessage(desSocket, tcpMessage);
        } catch (IOException e) {
            afterSendTcpMessageFail(tcpMessage, key);
            throw e;
        }
    }

    private void afterSendTcpMessageFail(TCPMessage tcpMessage, String key) {
        tcpMessage.setDescription(key);
        tcpMessage.setMessageType(TARGET_OFFLINE);

        partnerSocketMapForWrite.remove(key);
        partnerTcpInfoMap.remove(key);
    }

    private void preSendTcpMessage(TCPMessage tcpMessage, String key) {
        tcpSendingMap.put(tcpMessage.getId(), tcpMessage);
        if (partnerSocketMapForWrite.get(key) == null) {
            tcpMessage.setDescription(key);
            tcpMessage.setMessageType(TCPMessage.MessageType.TARGET_NOT_EXIST);
            throw new RuntimeException("目标连接未找到");
        }
    }

    private TCPMessage generateTcpSendMessageBy(String data) {
        InetSocketAddress localSocketAddress = (InetSocketAddress) serverSocket.getLocalSocketAddress();
        String address = generateKey(localSocketAddress.getAddress().getHostAddress(), localSocketAddress.getPort());
        return TCPMessage.builder().id(UUID.randomUUID().toString()).from(address).data(data).messageType(NORMAL).build();
    }

    @SneakyThrows
    private void sendAckTcpMessage(TCPMessage message) {
        message.setMessageType(ACK);
        Socket desSocket = partnerSocketMapForWrite.get(message.getFrom());
        if (desSocket == null) {
            throw new IOException("目标连接未找到");
        }
        try {
            doSendTcpMessage(desSocket, message);
        } catch (IOException e) {
            afterSendTcpMessageFail(message, message.getFrom());
            throw e;
        }
    }

    private void doSendTcpMessage(Socket desSocket, TCPMessage message) throws IOException, InterruptedException {
        desSocket.sendUrgentData(0xff);//TODO 一定程度保障
        Thread.sleep(1);
        OutputStream outputStream = desSocket.getOutputStream();
        outputStream.write((JSON.toJSONString(message) + "\n").getBytes());
    }


    public void createAndSaveConnectionBy(ClientTcpInfo clientTcpInfo) {
        boolean containsKey = partnerSocketMapForWrite.containsKey(clientTcpInfo.generateKey());
        if (!containsKey) {
            try {
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(clientTcpInfo.getIp(), clientTcpInfo.getPort()), 200);
                partnerSocketMapForWrite.put(clientTcpInfo.generateKey(), socket);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private ClientTcpInfo generateLocalClientTcpInfo() {
        InetSocketAddress localSocketAddress = (InetSocketAddress) serverSocket.getLocalSocketAddress();
        return ClientTcpInfo.builder()
                .id(UUID.randomUUID().toString())
                .ip(localSocketAddress.getAddress().getHostAddress())
                .port(localSocketAddress.getPort())
                .messageType(REPLY)
                .build();
    }


    @SneakyThrows
    public void stopTCPConnectionListener() {
        if (isCompleteOfTcpConnectionListener()) {
            serverSocket.close();
            tcpConnectionListenerThread.interrupt();
            isOkOfTCPConnectionListener = false;
        }
    }

    public void stopUdpListener() {
        if (isCompleteOfStartUDPMessageListener()) {
            datagramSocket.close();
            udpMessageListenerThread.interrupt();
            isOkOfUDPMessageListener = false;
        }
    }

    public void start(int udpListenerPort, int tcpListenerPort) {
        startUDPMessageListener(udpListenerPort);
        startTCPConnectionListener(tcpListenerPort);
        ClientTcpInfo clientTcpInfo = generateLocalClientTcpInfo();
        sendBroadcastMessage(clientTcpInfo);
        sendBroadcastMessage1(clientTcpInfo);//TODO 测试使用
    }

    @SneakyThrows
    private void sendBroadcastMessage1(ClientTcpInfo clientTcpInfo) {
        clientTcpInfo.setMessageType(BROADCAST);

        String jsonString = JSON.toJSONString(clientTcpInfo);
        byte[] jsonStringBytes = jsonString.getBytes();
        InetSocketAddress inetSocketAddress = new InetSocketAddress(BROADCAST_ADDR, 22222);
        DatagramPacket datagramPacket = new DatagramPacket(jsonStringBytes, jsonStringBytes.length, inetSocketAddress);
        datagramSocket.send(datagramPacket);

        postSendUdpMessage(clientTcpInfo);
    }

    @SneakyThrows
    public void shutdown() {
        stopUdpListener();
        stopTCPConnectionListener();
        partnerSocketMapForWrite.forEach((s, socket) -> {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        partnerSocketMap4Read.forEach((s, socket) -> {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public int getCountOfPartnerTcpInfo() {
        return partnerTcpInfoMap.size();
    }

    public int getCountOfPartnerTcpForWrite() {
        return partnerSocketMapForWrite.size();
    }

    public int getCountOfPartnerTcpForRead() {
        return partnerSocketMap4Read.size();
    }

    public int getTcpMessageQueueCount() {
        return tcpReceiveMessageQueue.size();
    }


    public int getSocket2ByteBufferCount() {
        return socket2ByteBuffer.size();
    }

    public int getTcpSendingMapCount() {
        return tcpSendingMap.size();
    }

    public ConcurrentHashMap<String, TCPMessage> getTcpSendingMap() {
        return tcpSendingMap;
    }

    private String getKeyBy(Socket accept) {
        InetSocketAddress remoteSocketAddress = (InetSocketAddress) accept.getRemoteSocketAddress();
        return generateKey(remoteSocketAddress.getHostString(), remoteSocketAddress.getPort());
    }

    private String generateKey(String ip, int port) {
        return ip + "|" + port;
    }


    private boolean isCompleteOfTcpConnectionListener() {
        return isOkOfTCPConnectionListener && serverSocket != null;
    }

    private boolean isContainPartnerTcpInfo(ClientTcpInfo clientTcpInfo) {
        return partnerTcpInfoMap.put(clientTcpInfo.generateKey(), clientTcpInfo) != null;
    }

    private boolean isSendBySelf(ClientTcpInfo clientTcpInfo) {
        return udpSendingMap.containsKey(clientTcpInfo.getId());
    }

    private boolean isCompleteOfStartUDPMessageListener() {
        return isOkOfUDPMessageListener && datagramSocket != null;
    }
}
