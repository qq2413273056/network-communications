package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetworkCommunicationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetworkCommunicationsApplication.class, args);
    }


}
