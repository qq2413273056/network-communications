package com.example.networkcommunicationsV1.codec;

import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.nio.ByteBuffer;

@NoArgsConstructor
public class Decode {


    private ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

    public String decode(@NonNull ByteBuffer byteBuffer) {
        byteBuffer.flip();
        int position = byteBuffer.position();
        int des;
        byteBuffer.mark();
        while (byteBuffer.position() < byteBuffer.limit()) {
            des = byteBuffer.position();
            byte b = byteBuffer.get();
            if (b == 10) {
                byteBuffer.reset();
                byte[] data = new byte[des - position];
                byteBuffer.get(data, 0, des - position);
                byteBuffer.clear();
                return new String(data);
            }
        }
        byteBuffer.limit(byteBuffer.capacity());
        return null;
    }


    public String decode(int read) {
        byteBuffer.put((byte) (read & 0xFF));
        return decode(byteBuffer);
    }

}
