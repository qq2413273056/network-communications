package com.example.networkcommunicationsV1.p2p.client;

import com.example.networkcommunicationsV1.codec.Decode;
import com.example.networkcommunicationsV1.entity.ClientTcpInfo;
import com.example.networkcommunicationsV1.exception.PartnerSocketCloseException;
import com.example.networkcommunicationsV1.service.TCPService;
import com.example.networkcommunicationsV1.service.UDPService;
import lombok.NonNull;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class Client {


    private UDPService udpService;
    private TCPService tcpService;
    private final int udpListenerPort;
    private final int udpDesPort;
    private final int tcpListenerPort;
    private LinkedBlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();
    private Decode decode = new Decode();

    public Client(int udpListenerPort, int desPort, int tcpListenerPort) {
        this.udpListenerPort = udpListenerPort;
        this.udpDesPort = desPort;
        this.tcpListenerPort = tcpListenerPort;
    }


    public List<ClientTcpInfo> getPartnerTcpInfo() {
        return new ArrayList<>(udpService.getMessageQueue());
    }

    public void start() {
        tcpService = new TCPService(tcpListenerPort);
        tcpService.startConnectionListener(byteBuffer -> {
            String decode = this.decode.decode(byteBuffer);
            if (decode != null) {
                messageQueue.add(decode);
            }
        });
        ServerSocket serverSocket = tcpService.getServerSocket();

        udpService = new UDPService();
        String hostAddress = serverSocket.getInetAddress().getHostAddress();//TODO 本地调试
        if (hostAddress.equals("0.0.0.0")) {
            hostAddress = "127.0.0.1";
        }
        ClientTcpInfo clientTcpInfo = ClientTcpInfo.builder().ip(hostAddress).port(serverSocket.getLocalPort()).build();
        udpService.openReceiveMessage(udpListenerPort, () -> udpService.sendBroadcastMessage(clientTcpInfo, udpDesPort));
        udpService.sendBroadcastMessage(clientTcpInfo, udpDesPort);
    }


    public int getTcpMessageCount() {
        return messageQueue.size();
    }

    public void shutdown() {
        tcpService.stopConnectionListener();
        tcpService.stopMessageListener();

        udpService.stopMessageListener();
    }

    public void stopMessageListener() {
        tcpService.stopMessageListener();
    }


    public void sendMessage(@NonNull ClientTcpInfo clientTcpInfo, @NonNull String msg) {
        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(clientTcpInfo.getIp(), clientTcpInfo.getPort()));
            socket.getOutputStream().write(msg.getBytes());
        } catch (IOException e) {
            udpService.getMessageQueue().remove(clientTcpInfo);
            e.printStackTrace();
            throw new PartnerSocketCloseException();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void sendMessage(String msg) {
        List<ClientTcpInfo> partnerTcpInfo = getPartnerTcpInfo();
        partnerTcpInfo.forEach(clientTcpInfo -> sendMessage(clientTcpInfo, msg));

    }

    public Socket getPartnerSocket(ClientTcpInfo clientTcpInfo) {
        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(clientTcpInfo.getIp(), clientTcpInfo.getPort()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return socket;
    }

    public void sendMessage(Socket partnerSocket, String msg) {
        try {
            partnerSocket.sendUrgentData(0xff);
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            partnerSocket.getOutputStream().write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            throw new PartnerSocketCloseException();
        } finally {
            try {
                partnerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
