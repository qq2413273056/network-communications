package com.example.networkcommunicationsV1.entity;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClientTcpInfo {
    private String id;
    private String ip;
    private Integer port;
    private MessageType messageType;

    public enum MessageType {
        BROADCAST,
        REPLY
    }

    public String generateKey() {
        return ip + "|" + port;
    }
}
