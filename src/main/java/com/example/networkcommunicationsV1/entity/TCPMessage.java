package com.example.networkcommunicationsV1.entity;

import lombok.*;

@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
public class TCPMessage {
    private String id;
    private String from;
    private String to;
    private String data;
    private MessageType messageType;
    private Integer needAckCount;
    private Integer ackCount;
    private String description;

    public void exchangeFromTo() {
        String from = this.getFrom();
        String to = this.getTo();
        this.setTo(from);
        this.setFrom(to);
    }

    public enum MessageType {
        NORMAL,
        ACK,
        TARGET_NOT_EXIST,
        TARGET_OFFLINE,
    }
}

