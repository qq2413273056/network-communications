package com.example.networkcommunicationsV1.service;

import com.alibaba.fastjson.JSON;
import com.example.networkcommunicationsV1.entity.ClientTcpInfo;
import lombok.Getter;
import lombok.NonNull;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;

@Getter
public class UDPService {

    public static final String BROADCAST_ADDR = "255.255.255.255";
    public static final int DESTINATION_BROADCAST_PORT = 9999;

    private final LinkedBlockingQueue<ClientTcpInfo> messageQueue = new LinkedBlockingQueue<>();
    private DatagramSocket listenerSocket;


    public void sendBroadcastMessage(@NonNull ClientTcpInfo clientTcpInfo, int desPort) {
        String jsonString = JSON.toJSONString(clientTcpInfo);
        byte[] jsonStringBytes = jsonString.getBytes();

        sendBroadcastMessage(jsonStringBytes, desPort);
    }

    private void sendBroadcastMessage(@NonNull byte[] bytes, int desPort) {
        sendMessage(bytes, BROADCAST_ADDR, desPort);
    }

    public void sendMessage(@NonNull ClientTcpInfo clientTcpInfo, String desHost, int desPort) {
        String jsonString = JSON.toJSONString(clientTcpInfo);
        byte[] jsonStringBytes = jsonString.getBytes();

        sendMessage(jsonStringBytes, desHost, desPort);
    }

    private void sendMessage(@NonNull byte[] bytes, String desHost, int desPort) {
        DatagramSocket datagramSocket = null;
        try {
            datagramSocket = new DatagramSocket();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(desHost, desPort);
            DatagramPacket sendDatagramPacket = new DatagramPacket(bytes, bytes.length, inetSocketAddress);
            datagramSocket.send(sendDatagramPacket);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Objects.requireNonNull(datagramSocket).close();//TODO  bug 不释放报错
        }
    }


    public void openReceiveMessage(int receivePort, Runnable callable) {
        try {
            listenerSocket = new DatagramSocket(new InetSocketAddress(receivePort));
            byte[] bytes = new byte[1024];
            DatagramPacket receiveDatagramPacket = new DatagramPacket(bytes, 1024);
            Thread listenerThread = new Thread(() -> {
                while (!listenerSocket.isClosed()) {
                    try {
                        listenerSocket.receive(receiveDatagramPacket);//UDP不存在沾包问题

                        ClientTcpInfo clientTcpInfo = datagramPacket2ClientTcpInfo(receiveDatagramPacket);

                        boolean isRepeat = isRepeatOfMessage(clientTcpInfo);
                        if (!isRepeat) {
                            messageQueue.add(clientTcpInfo);
                            callable.run();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            listenerThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isRepeatOfMessage(ClientTcpInfo clientTcpInfo) {
        return getMessageQueue().stream().anyMatch(clientTcpInfo1 -> {
            String s = clientTcpInfo1.toString();
            return s.equals(clientTcpInfo.toString());
        });
    }

    private ClientTcpInfo datagramPacket2ClientTcpInfo(DatagramPacket receiveDatagramPacket) {
        String messageStr = new String(receiveDatagramPacket.getData(), 0, receiveDatagramPacket.getLength());
        return JSON.parseObject(messageStr, ClientTcpInfo.class);
    }

    public void stopMessageListener() {
        if (listenerSocket != null) {
            listenerSocket.close();
        }
    }

}
