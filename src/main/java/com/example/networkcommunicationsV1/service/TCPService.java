package com.example.networkcommunicationsV1.service;

import com.google.common.collect.Sets;
import lombok.NonNull;
import lombok.SneakyThrows;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

public class TCPService {

    private ServerSocket serverSocket;
    private Set<Socket> partnerSocketSet = Sets.newConcurrentHashSet();

    private Thread messageListenerThread;
    private ConcurrentHashMap<Socket, ByteBuffer> socket2ByteBuffer = new ConcurrentHashMap<>();

    @SneakyThrows
    public TCPService(int servicePort) {
        serverSocket = new ServerSocket();
        serverSocket.setReuseAddress(true);
        serverSocket.bind(new InetSocketAddress(servicePort));
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    private void startConnectionListener(boolean startMessageListener, @NonNull Consumer<ByteBuffer> consumer) {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Thread connectionListenerThread = new Thread(() -> {
            countDownLatch.countDown();
            try {
                while (isOkOfServerSocket()) {
                    Socket partnerSocket = serverSocket.accept();

                    partnerSocketSet.add(partnerSocket);

                    if (startMessageListener) {
                        socket2ByteBuffer.put(partnerSocket, ByteBuffer.allocate(1024));
                        startMessageListener(partnerSocket, consumer);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        connectionListenerThread.start();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean isOkOfServerSocket() {
        return serverSocket != null && !serverSocket.isClosed();
    }

    public void startConnectionListener(Consumer<ByteBuffer> consumer) {
        startConnectionListener(true, consumer);
    }

    public void startConnectionListener() {
        startConnectionListener(true, byteBuffer -> {
        });
    }

    public int getCountOfPartnerSocket() {
        return partnerSocketSet.size();
    }

    public Set<Socket> getPartnerSocket() {
        return partnerSocketSet;
    }

    public void stopConnectionListener() {

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopMessageListener() {
        partnerSocketSet.forEach(socket -> {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    private void startMessageListener(Socket partnerSocket, Consumer<ByteBuffer> consumer) {
        messageListenerThread = new Thread(() -> {
            InputStream inputStream;
            try {
                while (true) {
                    inputStream = partnerSocket.getInputStream();
                    int read = inputStream.read();
                    if (read == -1) {
                        closePartnerSocket(partnerSocket);
                        return;
                    }
                    ByteBuffer byteBuffer = socket2ByteBuffer.get(partnerSocket);
                    byteBuffer.put((byte) (read & 0xFF));
                    consumer.accept(byteBuffer);
                }
            } catch (IOException e) {
                try {
                    closePartnerSocket(partnerSocket);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                e.printStackTrace();
            }
        });
        messageListenerThread.start();
    }

    private void closePartnerSocket(Socket partnerSocket) throws IOException {
        partnerSocket.close();
        partnerSocketSet.remove(partnerSocket);
    }

    public ByteBuffer getByteBufferOfSocket(@NonNull Socket socket) {
        return socket2ByteBuffer.get(socket);
    }

    @SneakyThrows
    public void sendMessage(InetSocketAddress inetSocketAddress, @NonNull String msg, TCPService tcpService1) {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Socket socket = new Socket();
        socket.connect(inetSocketAddress);
        new Thread(() -> {
            InputStream inputStream;
            countDownLatch.countDown();
            try {
                inputStream = socket.getInputStream();
                while (true) {
                    int read = inputStream.read();
                    if (read == -1) {
                        socket.close();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    socket.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

        }).start();
        countDownLatch.await();
        //TODO test
        if (tcpService1 != null) {

            tcpService1.stopMessageListener();
        }
        Thread.sleep(100);
        socket.getOutputStream().write(msg.getBytes());
        socket.close();
    }

}
