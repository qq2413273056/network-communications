package com.example.networkcommunicationsV1.cs;

import com.alibaba.fastjson.JSON;
import com.example.networkcommunicationsV1.codec.Decode;
import com.example.networkcommunicationsV1.codec.Encode;
import com.example.networkcommunicationsV1.entity.TCPMessage;
import com.example.networkcommunicationsV1.service.TCPService;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import static com.example.networkcommunicationsV1.entity.TCPMessage.MessageType.TARGET_OFFLINE;

public class Server {
    private final int listenerPort;
    private TCPService tcpService;
    private Decode decode = new Decode();
    private LinkedBlockingQueue<TCPMessage> messageQueue = new LinkedBlockingQueue<>();
    private ConcurrentHashMap<String, TCPMessage> sendingMap = new ConcurrentHashMap<>();

    public Server(int listenerPort) {
        this.listenerPort = listenerPort;
    }

    public void startConnectionListener() {
        tcpService = new TCPService(listenerPort);
        tcpService.startConnectionListener(byteBuffer -> {
            String decode = this.decode.decode(byteBuffer);
            handleMessage(decode);
        });
    }

    private void handleMessage(String decode) {
        if (decode != null) {
            TCPMessage message = JSON.parseObject(decode, TCPMessage.class);
            if (isAckMessage(message)) {
                message.exchangeFromTo();
                sendingMap.remove(message.getId());
                sendMessage(message);
                return;
            }
            messageQueue.add(message);
            sendMessageNeedAck(message);
        }
    }

    public int getSendingMessageCount() {
        return sendingMap.size();
    }

    private boolean isAckMessage(TCPMessage message) {
        return message.getMessageType() == TCPMessage.MessageType.ACK;
    }

    public void stopConnectionListener() {
        if (tcpService != null) {
            tcpService.stopConnectionListener();
        }
    }

    public int getClientCount() {
        return tcpService.getPartnerSocket().size();
    }


    public BlockingQueue<TCPMessage> getMessageQueue() {
        return messageQueue;
    }

    public void stopMessageListener() {
        tcpService.stopMessageListener();
    }


    public void sendMessage(TCPMessage msg) {
        List<Socket> desClientByMessage = findDesClientByMessage(msg);

        if (desClientByMessage.size() == 0) {
            msg.setMessageType(TARGET_OFFLINE);
            msg.exchangeFromTo();
            List<Socket> findSendClient = findDesClientByMessage(msg);
            if (findSendClient.size() != 0) {
                sendMessage(msg);
            }
            return;
        }
        desClientByMessage.forEach(socket -> {
            try {
                if (StringUtils.isEmpty(msg.getTo()) || socket.getRemoteSocketAddress().toString().equals(msg.getTo())) {
                    socket.getOutputStream().write(Encode.encode(msg));
                    socket.getOutputStream().flush();
                }
            } catch (SocketException e) {
                e.printStackTrace();
                msg.setMessageType(TARGET_OFFLINE);
                msg.exchangeFromTo();
                sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendMessageNeedAck(TCPMessage msg) {
        List<Socket> desClientByMessage = findDesClientByMessage(msg);
        msg.setNeedAckCount(desClientByMessage.size());

        sendingMap.put(msg.getId(), msg);

        sendMessage(msg);
    }

    private List<Socket> findDesClientByMessage(TCPMessage tcpMessage) {
        return tcpService.getPartnerSocket().stream().filter(socket -> StringUtils.isEmpty(tcpMessage.getTo()) || socket.getRemoteSocketAddress().toString().equals(tcpMessage.getTo())).collect(Collectors.toList());
    }
}
