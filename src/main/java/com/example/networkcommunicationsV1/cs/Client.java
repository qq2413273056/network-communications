package com.example.networkcommunicationsV1.cs;

import com.alibaba.fastjson.JSON;
import com.example.networkcommunicationsV1.codec.Decode;
import com.example.networkcommunicationsV1.codec.Encode;
import com.example.networkcommunicationsV1.entity.TCPMessage;
import lombok.SneakyThrows;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

import static com.example.networkcommunicationsV1.entity.TCPMessage.MessageType.NORMAL;

public class Client {
    private final int desPort;
    private final String desIp;
    private Socket socket;
    private ConcurrentHashMap<String, TCPMessage> sendingMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, TCPMessage> targetNotExistMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, TCPMessage> targetOffLineMap = new ConcurrentHashMap<>();
    private Thread listenerThread;
    private Decode decode;

    public Client(String desIp, int desPort) {
        this.desPort = desPort;
        this.desIp = desIp;
    }

    @SneakyThrows
    public void start() {
        decode = new Decode();
        socket = new Socket();
        socket.connect(new InetSocketAddress(desIp, desPort));
        listenerThread = new Thread(() -> {
            InputStream inputStream = null;
            try {
                inputStream = socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                    assert inputStream != null;
                    int read = inputStream.read();
                    if (read == -1) {
                        socket.close();
                        return;
                    }
                    String decode = this.decode.decode(read);

                    handleMessage(decode);
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        socket.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    return;
                }
            }
        });
        listenerThread.start();
    }

    private void handleMessage(String decode) {
        if (!StringUtils.isEmpty(decode)) {
            TCPMessage message = JSON.parseObject(decode, TCPMessage.class);
            switch (message.getMessageType()) {
                case ACK: {
                    int ackCount = sendingMap.get(message.getId()).getAckCount();
                    message.setAckCount(++ackCount);
                    if (message.getNeedAckCount() == message.getAckCount()) {
                        sendingMap.remove(message.getId());
                    } else {
                        sendingMap.put(message.getId(), message);
                    }
                }
                break;
                case TARGET_NOT_EXIST:
                    targetNotExistMap.put(message.getId(), sendingMap.remove(message.getId()));
                    break;
                case TARGET_OFFLINE:
                    targetOffLineMap.put(message.getId(), sendingMap.get(message.getId()));
                    break;
                case NORMAL: {
                    message.setMessageType(TCPMessage.MessageType.ACK);
                    sendMessage(message);
                }
            }
        }
    }


    @SneakyThrows
    public void shutdown() {
        if (socket != null) {
            socket.close();
        }
        if (listenerThread != null) {
            listenerThread.interrupt();
        }
    }

    @SneakyThrows
    public void sendMessage(TCPMessage tcpMessage) {
        if (tcpMessage.getMessageType() == null) {
            tcpMessage.setMessageType(NORMAL);
            sendingMap.put(tcpMessage.getId(), tcpMessage);
        }
        socket.getOutputStream().write(Encode.encode(tcpMessage));
    }

    public String getLocalSocketAddress() {
        return socket.getLocalSocketAddress().toString();
    }

    public int getMessageCountOfSending() {
        return sendingMap.size();
    }

    public int getMessageCountOfTargetOffline() {
        return targetOffLineMap.size();
    }
}
