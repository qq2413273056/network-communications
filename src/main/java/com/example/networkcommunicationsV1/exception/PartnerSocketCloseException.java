package com.example.networkcommunicationsV1.exception;

public class PartnerSocketCloseException extends RuntimeException {
    public PartnerSocketCloseException() {
        super("PartnerSocketCloseException");
    }
}
